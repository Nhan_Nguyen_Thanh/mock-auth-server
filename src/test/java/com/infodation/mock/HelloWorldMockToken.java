package com.infodation.mock;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.infodation.mock.common.AbstractControllerTest;
import com.infodation.mock.utils.Common;

import lukastosic.mocks.MockService;

//import lukastosic.mocks.MockService;

public class HelloWorldMockToken extends AbstractControllerTest {

	private MockService mockService = new MockService();

	//
	//// @InjectMocks
	//// private HelloWorldController helloworldcontroller=new
	// HelloWorldController();
	//
	@Before
	public void setup() {
		super.setUp();
		prepareMockServer();
		setupExternalAPIMockAddress();
	}

	//
	//
	public void prepareMockServer() {
		mockService.StartMockServer();
		mockService.LoadMapppingOptions(true);
		mockService.SetMapping("AUTHENTICATION_SERVICE_STANDARD_SETUP");
	}

	private void setupExternalAPIMockAddress() {
		Common.EXTERNAL_API_ADDRESS = "http://localhost:" + mockService.GetHttpPort() + "/verify";
	}

	//
	@Test
	public void testVerifyWithNoToken() throws Exception {
		// mockMvc.perform(post("/helloworld2"))
		// .andExpect(status().isBadRequest());
		String url = "/helloworld2";
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(url)).andReturn();
		int status = result.getResponse().getStatus();
	}

	@Test
	public void testVerifyWithBadToken() throws Exception {
		// mockMvc.perform(post("/helloworld2")
		// .header("Token", "bad-token"))
		// .andExpect(status().isBadRequest());
		String url = "/helloworld2";
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(url).header("Token", "bad-token")).andReturn();
		int status = result.getResponse().getStatus();
	}

	//
	@Test
	public void testVerifyWithGoodToken() throws Exception {
		String url = "/helloworld2";
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(url).header("Token", "good-token")).andReturn();
		int status = result.getResponse().getStatus();
		// Assert.assertEquals("Expected HTTP status 200", 200, status);
	}

	@After
	public void closeMockService() {
		mockService.StopMockServer();
	}
}
