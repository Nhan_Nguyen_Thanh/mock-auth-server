package com.infodation.mock;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.common.net.MediaType;
import com.infodation.mock.common.AbstractControllerTest;
import com.infodation.mock.utils.Common;

import lukastosic.mocks.MockService;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration({"classpath:HelloMock-context.xml"})
public class HelloWorldMockLogin extends AbstractControllerTest{
	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;
	private MockService mockService = new MockService();
//	
//	@InjectMocks
//    private HelloWorldController helloworldcontroller=new HelloWorldController();
//	
	@Before
	public void setup() {
		super.setUp();
		prepareMockServer();
		setupExternalAPIMockAddress();
	}
//	
//
	public void prepareMockServer() {
		mockService.StartMockServer();
		mockService.LoadMapppingOptions(true);
		mockService.SetMapping("AUTHENTICATION_SERVICE_STANDARD_SETUP");
	}
	
	private void setupExternalAPIMockAddress() {
		Common.EXTERNAL_API_ADDRESS = "http://localhost:" + mockService.GetHttpPort() + "/login";
	}
	
	@Test
	public void testLoginWithGoodParam() throws Exception {
		String url = "/helloworld1";
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(url)
							  .param("username", "admin1")
							  .param("password", "123"))
							  .andReturn();
		int status = result.getResponse().getStatus();
	}

	@Test
	public void testLoginWithBadParam() throws Exception {
		String url = "/helloworld1";
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(url)
							  .param("username", "admin1")
							  .param("password", "1234"))
							  .andReturn();
		int status = result.getResponse().getStatus();
	}

	@Test
	public void testLoginWithBadRequest() throws Exception {
		String url = "/helloworld1";
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(url))
							  .andReturn();
		int status = result.getResponse().getStatus();
	}
//	
	@After
	public void closeMockService()  {
		mockService.StopMockServer();
	}
}
