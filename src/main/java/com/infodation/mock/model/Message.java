package com.infodation.mock.model;

public class Message {
	String name;
	String text;
	int a;
	int b;

	public Message(String name, String text) {
		this.name = name;
		this.text = text;
	}
	
	public Message(int a, int b){
		this.a=a;
		this.b=b;
	}
	
	public Message(){
		
	}

	public String getName() {
		return name;
	}

	public String getText() {
		return text;
	}
	
	public String toString(){
		return name + text;
	}
	
	public String sum(){
		return "a+b = " + Integer.toString(a+b) ;
	}

}
