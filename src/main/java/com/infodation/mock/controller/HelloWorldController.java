package com.infodation.mock.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.UriBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.infodation.mock.model.ResponseReturn;
import com.infodation.mock.utils.Common;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import lukastosic.mocks.MockService;

@RestController
public class HelloWorldController {
	@RequestMapping("/hello")
	public String welcome() {// Welcome page, non-rest`
		return "WELCOME TO THE WORLD, MEN.";
	}

	private MockService mockService = new MockService();

	@RequestMapping(value = "/hello", method = RequestMethod.POST)
	public ResponseEntity<ResponseReturn<String>> HelloWorldAction() {
		ResponseReturn<String> response = new ResponseReturn<String>();
		response.setMessage("test hello success");
		return new ResponseEntity<ResponseReturn<String>>(response, HttpStatus.OK);
	}

	// @RequestMapping("/hello/{player}")
	// public String message(@PathVariable String player) {// REST Endpoint.
	//
	// Message msg = new Message(player, " Hello " + player);
	// return msg.toString();
	// }

	@RequestMapping(value = "helloworld1")
	public ResponseEntity<ResponseReturn<String>> Hello1(HttpServletRequest request) throws ClientProtocolException, IOException {
		ResponseReturn<String> response = new ResponseReturn<String>();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		if (username == null && password == null) {
			response.setMessage("Authorization token is not provided");
			return new ResponseEntity<ResponseReturn<String>>(response, HttpStatus.FORBIDDEN);
		}

		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(UriBuilder.fromUri(Common.EXTERNAL_API_ADDRESS).build());
		ClientResponse authResponse = service.queryParam("username", username).queryParam("password", password).post(ClientResponse.class);
		try {
			if (authResponse.getStatus() == 200) {
				response.setMessage("Good login");
				return new ResponseEntity<ResponseReturn<String>>(response, HttpStatus.OK);

			} else {
				response.setMessage("Failed login, wrong parameters");
				return new ResponseEntity<ResponseReturn<String>>(response, HttpStatus.FORBIDDEN);
			}
		} catch (Exception ex) {
			response.setMessage("Login is not success");
			return new ResponseEntity<ResponseReturn<String>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// @RequestMapping(value = "helloworld1")
	// public ResponseEntity<ResponseReturn<String>> Hello1() {
	// ResponseReturn<String> response = new ResponseReturn<String>();
	// String username=request.getParameter("username");
	// String password=request.getParameter("password");
	// MockService mockService = new MockService();
	// HttpClient client = new DefaultHttpClient();
	// HttpPost post = new HttpPost("http://localhost:" +
	// mockService.GetHttpPort() + "/login");
	// HttpResponse response1 = client.execute(post);
	// return new ResponseEntity<ResponseReturn<String>>(HttpStatus.OK);
	// }
	//
	@RequestMapping(value = "helloworld2", method = RequestMethod.POST)
	public ResponseEntity<ResponseReturn<String>> Hello2(HttpServletRequest request)
			throws ClientProtocolException, IOException {
		ResponseReturn<String> response = new ResponseReturn<String>();
		String token = request.getHeader("Token");

		// Make sure that token is even provided
		if (token == null) {
			response.setMessage("Authorization token is not provided");
			return new ResponseEntity<ResponseReturn<String>>(response, HttpStatus.FORBIDDEN);
		}

		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(UriBuilder.fromUri(Common.EXTERNAL_API_ADDRESS).build());
		ClientResponse authResponse = service.header("Token", token).post(ClientResponse.class);
		try {
			if (authResponse.getStatus() == 200) {
				response.setMessage("Good roken confirmation");
				return new ResponseEntity<ResponseReturn<String>>(response, HttpStatus.OK);

			} else {
				response.setMessage("Bad token confirmation");
				return new ResponseEntity<ResponseReturn<String>>(response, HttpStatus.FORBIDDEN);
			}
		} catch (Exception ex) {
			response.setMessage("Call to external REST API server not succeded");
			return new ResponseEntity<ResponseReturn<String>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// @RequestMapping(value = "helloworld2", method=RequestMethod.POST)
	// public String Hello2(@RequestParam("token") String token,
	// HttpServletRequest request) throws ClientProtocolException, IOException {
	// String Token = request.getHeader("Token");
	// MockService mockService = new MockService();
	// mockService.SetMapping("AUTHENTICATION_SERVICE_STANDARD_SETUP");
	// HttpClient client = new DefaultHttpClient();
	// HttpPost post = new HttpPost("http://localhost:" +
	// mockService.GetHttpPort() + "/verify");
	// ClientResponse authResponse = ((WebResource) client).header("Token",
	// token).post(ClientResponse.class);
	// HttpResponse response = client.execute(post);
	// return "return " + token;
	// }

	// @RequestMapping(value = "/hello/{a}/{b}", method = RequestMethod.GET)
	// public String sum(@PathVariable("a") int a, @PathVariable("b") int b) {
	// Message sumab = new Message(a, b);
	// return sumab.sum();
	// }
}
